<?php

namespace Drupal\awesome\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class DeleteContactsForm.
 *
 * @package Drupal\awesome\Form
 */
class DeleteContactsForm extends ConfirmFormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'delete_contact_form';
    }

    public $cid;

    /**
     * return string
     */
    public function getQuestion()
    {
        return t('Estás apunto de eliminar el contacto');
    }

    /**
     * return Url
     */
    public function getCancelUrl()
    {
        return new Url('awesome.display_table_controller_list');
    }

    /**
     * return string
     */
    public function getDescription()
    {
        return t('Estás seguro, seguro, seguo?!');
    }

    /**
     * {@inheritdoc}
     */
    public function getConfirmText()
    {
        return t('Eliminar contacto!');
    }

    /**
     * {@inheritdoc}
     */
    public function getCancelText()
    {
        return t('Cancelar');
    }

    /**
     * {@inheritdoc}
     */

    public function buildForm(array $form, FormStateInterface $form_state, $cid = null)
    {
        $this->id = $cid;
        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $query = \Drupal::database();
        $query->delete('contacts')
            ->condition('id', $this->id)
            ->execute();
        drupal_set_message("succesfully deleted");
        $form_state->setRedirect('awesome.display_table_controller_list');
    }
}
