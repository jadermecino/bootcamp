<?php
namespace Drupal\awesome\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContactForm.
 *
 * @package Drupal\awesome\Form
 */
class ContactsForm extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        // Nombre del formulario
        return 'edit_contact_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $conn = Database::getConnection();
        $record = array();

        if (isset($_GET['contact_id'])) {
            $query = $conn->select('contacts', 'm')
                ->condition('id', $_GET['contact_id'])
                ->fields('m');
            $record = $query->execute()->fetchAssoc();
        }
        // Definimos los campos
        $form['nombre'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Nombre'),
            '#required' => true,
            '#default_value' => (isset($record['name']) && $_GET['contact_id']) ? $record['name'] : '',
        ];

        $form['birthdate'] = [
            '#type' => 'date',
            '#title' => $this->t('birthdate'),
            '#required' => true,
            '#default_value' => (isset($record['birthdate']) && $_GET['contact_id']) ? $record['birthdate'] : '',
        ];

        $form['sexo'] = [
            '#type' => 'select',
            '#title' => $this->t('Sexo'),
            '#options' => [
                '' => 'Seleccione',
                'F' => $this->t('Femenino'),
                'M' => $this->t('Masculino'),
            ],
            '#default_value' => (isset($record['gender']) && $_GET['contact_id']) ? $record['gender'] : '',
            '#required' => true,
        ];

        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('E-mail'),
            '#required' => true,
            '#default_value' => (isset($record['email']) && $_GET['contact_id']) ? $record['email'] : '',
        ];

        $form['mobilenumber'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Número de teléfono'),
            '#required' => true,
            '#default_value' => (isset($record['mobilenumber']) && $_GET['contact_id']) ? $record['mobilenumber'] : '',
        ];

        $form['location'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Dirección'),
            '#required' => true,
            '#default_value' => (isset($record['location']) && $_GET['contact_id']) ? $record['location'] : '',
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => (isset($_GET['contact_id'])) ? $this->t('Actualizar contacto') : $this->t('Crear contacto'),
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        // Hacemos las validaciones necesarias
        if (empty($form_state->getValue('nombre'))) {
            $form_state->setErrorByName('nombre', $this->t('Es necesario ingresar el nombre'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Mostrar resultados al enviar el formulario en un mensaje de drupal
        /*foreach ($form_state->getValues() as $key => $value) {
        drupal_set_message($key . ': ' . $value);
        }*/
        $field = $form_state->getValues();
        $name = $field['nombre'];
        $birthdate = $field['birthdate'];
        $gender = $field['sexo'];
        $email = $field['email'];
        $mobilenumber = $field['mobilenumber'];
        $location = $field['location'];
        if (isset($_GET['contact_id'])) {
            $field = [
                'name' => $name,
                'birthdate' => $birthdate,
                'gender' => $gender,
                'email' => $email,
                'mobilenumber' => $mobilenumber,
                'location' => $location,
            ];
            $query = \Drupal::database();
            $query->update('contacts')
                ->fields($field)
                ->condition('id', $_GET['contact_id'])
                ->execute();
            drupal_set_message("succesfully updated");
            $form_state->setRedirect('awesome.display_table_controller_list');
        } else {
            try {
                $field = [
                    // 'id' => null,
                    'name' => $name,
                    'birthdate' => $birthdate,
                    'gender' => $gender,
                    'email' => $email,
                    'mobilenumber' => $mobilenumber,
                    'location' => $location,
                ];
                $query = \Drupal::database();
                $query->insert('contacts')
                    ->fields(array_keys($field))
                    ->values(array_values($field))
                    ->execute();
                drupal_set_message("succesfully saved");
                $form_state->setRedirect('awesome.display_table_controller_list');
                // $response = new RedirectResponse("/contacts/list");
                // $response->send();
            } catch (Exception $e) {
                \Drupal::logger('widget')->error($e->getMessage());
                drupal_set_message(t('Ocurrió un error ingresando el contacto.'), 'error');
            }
        }
    }
}
