<?php
namespace Drupal\awesome\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CreateContactForm extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        // Nombre del formulario
        return 'create_contact_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {

        // Definimos los campos
        $form['nombre'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Nombre'),
            '#required' => true,
        ];
        
        $form['sexo'] = [
            '#type' => 'select',
            '#title' => $this->t('Sexo'),
            '#options' => [
                '' => 'Seleccione',
                'F' => $this->t('Femenino'),
                'M' => $this->t('Masculino'),
            ],
            '#required' => true,
        ];


        $form['location'] = [
            '#type' => 'text_format',
            '#title' => $this->t('Dirección'),
            '#required' => true,
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Crear contacto'),
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {

        // Hacemos las validaciones necesarias
        if (empty($form_state->getValue('nombre'))) {
            $form_state->setErrorByName('nombre', $this->t('Es necesario ingresar el nombre'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Mostrar resultados al enviar el formulario en un mensaje de drupal
        foreach ($form_state->getValues() as $key => $value) {
            drupal_set_message($key . ': ' . $value);
        }
    }
}
