<?php

namespace Drupal\awesome\Controller;

use Drupal\Core\Controller\ControllerBase;


/**
 * Returns responses for todos routes.
 */
class TodosController extends ControllerBase {

  /**
   * @return mixed
   */
  public function overview() {

    $build['hello'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Hello World'),
    ];

    return $build;

  }

   
  public function createContact() {
    // Utilizamos el formulario
    $form = $this->formBuilder()->getForm('Drupal\awesome\Form\CreateContactForm');
            
    // Le pasamos el formulario y demás a la vista (tema configurado en el module)
    return [
        '#theme' => 'create_contact',
        '#titulo' => $this->t('Crear contactos'),
        '#descripcion' => 'Formulario para crear un nuevo contacto en Drupal 8',
        '#formulario' => $form
    ];
  }
       
    

}
