<?php

namespace Drupal\awesome\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;

/**
 * Returns responses for todos routes.
 */
class ContactosController extends ControllerBase
{
    /**
     * contactList.
     *
     * @return string
     *   Return string.
     */
    public function contactList()
    {
        $header_table = [
            'id' => t('consecutivo'),
            'name' => t('Nombre'),
            'birthdate' => t('Fecha de nacimiento'),
            'gender' => t('Género'),
            'email' => t('E-mail'),
            'mobilenumber' => t('Número telefónico'),
            'location' => t('Dirección'),
            'editar' => t('Editar'),
            'eliminar' => t('Eliminar'),
        ];
        //select records from table
        $query = \Drupal::database()->select('contacts', 'm');
        $query->fields('m', ['id', 'name', 'birthdate', 'gender', 'email', 'mobilenumber', 'location']);
        $results = $query->execute()->fetchAll();
        $rows = array();
        foreach ($results as $data) {
            $delete = Url::fromUserInput('/contacts/form/delete/' . $data->id);
            $edit = Url::fromUserInput('/contacts/form?contact_id=' . $data->id);
            //print the data from table
            $rows[] = array(
                'id' => $data->id,
                'name' => $data->name,
                'birthdate' => $data->birthdate,
                'gender' => $data->gender,
                'email' => $data->email,
                'mobilenumber' => $data->mobilenumber,
                'location' => $data->location,
                \Drupal::l('Editar', $edit),
                \Drupal::l('Eliminar', $delete),
            );
        }
        //display data in site
        $form['table'] = [
            '#type' => 'table',
            '#header' => $header_table,
            '#rows' => $rows,
            '#empty' => t('No contacts found'),
        ];
        return $form;
    }

    public function createContact()
    {
        // Utilizamos el formulario
        $form = $this->formBuilder()->getForm('Drupal\awesome\Form\CreateContactForm');

        // Le pasamos el formulario y demás a la vista (tema configurado en el module)
        return [
            '#theme' => 'create_contact',
            '#titulo' => $this->t('Crear contactos'),
            '#descripcion' => 'Formulario para crear un nuevo contacto en Drupal 8',
            '#formulario' => $form,
        ];
    }

}