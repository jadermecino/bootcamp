<?php
namespace Drupal\awesome\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ContactsBlock' block.
 *
 * @Block(
 *  id = "awesome_contacts_block",
 *  admin_label = @Translation("Contacts block"),
 * )
 */
class ContactsBlock extends BlockBase
{
    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $form = \Drupal::formBuilder()->getForm('Drupal\mydata\Form\ContactsForm');
        return $form;
    }
}
