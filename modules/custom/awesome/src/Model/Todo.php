<?php

namespace Drupal\awesome\Model;

use Drupal\Core\Database\Connection;
use Psr\Log\LoggerInterface;

class Todo implements TodoInterface {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a Todo object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   * @param \Psr\Log\LoggerInterface $logger
   */
  public function __construct(Connection $connection, LoggerInterface $logger) {
    $this->connection = $connection;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function load($id = NULL) {
    if (!$id) {
      throw new \Exception('Id not found', 500);
    }
    return $this->connection
      ->query('SELECT t.*, u.uid FROM {' . self::TABLE . '} t LEFT JOIN {users} u ON u.uid = t.uid WHERE t.tid = :id', ['id' => $id])
      ->fetchObject();
  }

  /**
   * {@inheritdoc}
   */
  public function insert(array $data = []) {
    return $this->connection
      ->insert(self::TABLE)
      ->fields($data)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function update($id = NULL, array $fields = []) {
    if (!$id) {
      throw new \Exception('Id not found', 500);
    }
    return $this->connection
      ->update(self::TABLE)
      ->fields($fields)
      ->condition('tid', $id, '=')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function delete($id = NULL) {
    if (!$id) {
      throw new \Exception('Id not found', 500);
    }
    return $this->connection
      ->delete(self::TABLE)
      ->condition('tid', $id, '=')
      ->execute();
  }

}
