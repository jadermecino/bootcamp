<?php

namespace Drupal\awesome\Model;

interface TodoInterface {

  /**
   * Table name.
   */
  const TABLE = 'todo';

  /**
   * @param int $id
   *
   * @return mixed
   * @throws \Exception
   */
  public function load($id = NULL);

  /**
   * @param array $data
   *
   * @return mixed
   * @throws \Exception
   */
  public function insert(array $data = []);

  /**
   * @param int $id
   * @param array $fields
   *
   * @return mixed
   * @throws \Exception
   */
  public function update($id = NULL, array $fields = []);

  /**
   * @param int $id
   *
   * @return mixed
   * @throws \Exception
   */
  public function delete($id = NULL);

}
